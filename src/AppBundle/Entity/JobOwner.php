<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;


/**
 * JobOwner
 *
 * @ORM\Table(name="job_owner")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\JobOwnerRepository")
 */
class JobOwner extends User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $society;


    public function __construct()
    {
        parent::__construct();
        $this->roles = array('ROLE_JOBOWNER');
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getSociety()
    {
        return $this->society;
    }

    public function setSociety($society)
    {
        $this->society = $society;
    }
}


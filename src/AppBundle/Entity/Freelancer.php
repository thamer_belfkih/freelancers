<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;

/**
 * Freelancer
 *
 * @ORM\Table(name="freelancer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FreelancerRepository")
 */
class Freelancer extends User
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;


    /**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $name;


    public function __construct()
    {
        parent::__construct();
        $this->roles = array('ROLE_FREELANCER');
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name  = $name;
    }
}


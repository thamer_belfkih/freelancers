<?php



namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;




class JobOwnerController extends Controller{


	public function registerAction()
    {
        return $this->container
                    ->get('pugx_multi_user.registration_manager')
                    ->register('AppBundle\Entity\JobOwner');
                    
    }

    /**
    * @Route("/job-owner/login", name="login job-owner")
    */
    public function loginAction()
    {
        return $this->render('jobOwner/login.html.twig');
    }



    /**
    * @Route("/jobowner/user", name="login job-owner")
    */
    public function userData()
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();

        die($usr);
    }
}
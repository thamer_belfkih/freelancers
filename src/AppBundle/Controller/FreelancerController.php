<?php 


namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class FreelancerController extends Controller{


	
	public function registerAction(Request $request)
    {
        return $this->container
                    ->get('pugx_multi_user.registration_manager')
                    ->register('AppBundle\Entity\Freelancer');
    }

    /**
    * @Route("/freelancer/login", name="login freelancer")
    */
    public function loginAction()
    {
    	return $this->render('freelancer/login.html.twig');
    }


}
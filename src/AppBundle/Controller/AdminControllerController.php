<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AdminControllerController extends Controller
{
    

	 /**
    * @Route("/admin", name="login admin")
    */
    public function loginAction()
    {
        return $this->render('admin/login.html.twig');
    }

}
